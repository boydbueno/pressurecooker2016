﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Pickup : MonoBehaviour {

    public GameManager.PickupType pickupType;

    void OnTriggerEnter(Collider collider) {
        GameManager.instance.Pickup(pickupType, collider.GetComponent<Player>());
        Destroy(gameObject);
    }

    void Update() {
        transform.Rotate(Vector3.up * (100 * Time.deltaTime), Space.World);
    }
}
