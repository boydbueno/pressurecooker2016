﻿using UnityEngine;
using System.Collections;

public class Course : MonoBehaviour {

    public void Move(float speed) {
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed);
    }
}
