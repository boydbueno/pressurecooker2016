﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour {

    public int index;

    public LayerMask otherPlayerLayerMask;
    public LayerMask obstacleLayerMask;

    private float newXPosition;

    private float prevZPosition;

    private float moveDistance = 10;
    private float movementSpeed = 100;
    private float zCorrectSpeed = 20;

    private float jumpForce = 2800;
    private float enlargedJumpForce = 12000;
    private float shrunkJumpForce = 1700;

    private float distanceToGround;

    private Player otherPlayer;

    private bool isEnlarged;
    private bool isShrunk;

    void Start() {
        newXPosition = transform.position.x;
        distanceToGround = GetComponent<Collider>().bounds.extents.y;
        otherPlayer = GameManager.instance.GetOtherPlayer(this);
    }

    void Update() {
        float step = movementSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(newXPosition, transform.position.y, transform.position.z), step);
        
        if (IsMovingRight() && IsHittingSomeoneRight() && !otherPlayer.IsMovingRight()) {
            otherPlayer.MoveRight();
        }
        
        if (IsMovingLeft() && IsHittingSomeoneLeft() && !otherPlayer.IsMovingLeft()) {
            otherPlayer.MoveLeft();
        }

        if (Math.Round(transform.position.z) != 0) {
            if (transform.position.z == prevZPosition) {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 0), zCorrectSpeed * Time.deltaTime);
            }
        }

        prevZPosition = transform.position.z;
    }

    public void MoveLeft() {
        if (isObstacleLeft()) return;
        newXPosition = newXPosition - moveDistance;
        if (HasSomeoneOnTop()) {
            otherPlayer.MoveLeft();
        }
    }

    public void MoveRight() {
        if (IsObstacleRight()) return;
        newXPosition = newXPosition + moveDistance;
        if (HasSomeoneOnTop()) {
            otherPlayer.MoveRight();
        }
    }

    public void Jump() {
        if (!IsGrounded()) {
            return;
        }

        float jumpForce = this.jumpForce;

        if (isEnlarged) {
            jumpForce = enlargedJumpForce;
        } else if (isShrunk) {
            jumpForce = shrunkJumpForce;
        }
        
        GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpForce, 0));
    }

    public void SetPosition(Vector3 position) {
        newXPosition = position.x;
        transform.position = position;
    }

    public bool IsMovingRight() {
        return newXPosition > transform.position.x;
    }

    public bool IsMovingLeft() {
        return newXPosition < transform.position.x;
    }

    public void Enlarge() {
        transform.localScale = new Vector3(8, 8, 8);
        GetComponent<Rigidbody>().mass = 4;
        isEnlarged = true;
    }

    public void Shrink() {
        transform.localScale = new Vector3(2, 2, 2);
        GetComponent<Rigidbody>().mass = 0.5f;
        isShrunk = true;
    }

    public void ReturnToNormal() {
        // Todo: Define these on top
        transform.localScale = new Vector3(4, 4, 4);
        GetComponent<Rigidbody>().mass = 1f;
        // Todo: Turn these into enums
        isShrunk = false;
        isEnlarged = false;
    }

    private bool IsGrounded() {
        if (isEnlarged) {
            return Physics.Raycast(transform.position, -Vector3.up, distanceToGround * 2 + 0.4f);
        }

        if (isShrunk) {
            return Physics.Raycast(transform.position, -Vector3.up, distanceToGround / 2 + 0.4f);
        }
        return Physics.Raycast(transform.position, -Vector3.up, distanceToGround + 0.4f);
    }

    private bool HasSomeoneOnTop() {
        if (isEnlarged) {
            return Physics.Raycast(transform.position, Vector3.up, distanceToGround * 2 + 1f);
        }

        if (isShrunk) {
            return Physics.Raycast(transform.position, Vector3.up, distanceToGround / 2 + 1f);
        }
        return Physics.Raycast(transform.position, Vector3.up, distanceToGround + 1f);
    }

    private bool IsHittingSomeoneRight() {
        return Physics.Raycast(transform.position, Vector3.right, distanceToGround, otherPlayerLayerMask);
    }

    private bool IsHittingSomeoneLeft() {
        return Physics.Raycast(transform.position, -Vector3.right, distanceToGround, otherPlayerLayerMask);
    }

    private bool IsObstacleRight() {
        return Physics.Raycast(transform.position, Vector3.right, distanceToGround * 2, obstacleLayerMask);
    }

    private bool isObstacleLeft() {
        return Physics.Raycast(transform.position, -Vector3.right, distanceToGround * 2, obstacleLayerMask);
    }
}
