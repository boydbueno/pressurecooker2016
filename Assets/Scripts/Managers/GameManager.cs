﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using XInputDotNetPure;

public class GameManager : MonoBehaviour {

    public List<Player> players = new List<Player>();
    public Course course;

    public static GameManager instance;

    public PowerupType powerupType;

    public enum PowerupType {
        always,
        pickup,
        pickupRumble
    }

    public enum PickupType {
        swap,
        resize,
        timeStop
    }

    public List<PickupType> pickups = new List<PickupType>();
    public List<string> pickupPattern = new List<string>();

    public float courseMovementSpeed = 1f;

    private float normalCourseMovementSpeed = 1;
    private float minCourseMovementSpeed = 0.05f;
    private bool IsTimeStopping = false;
   
    /// <summary>
    /// The time it takes to stop the time
    /// </summary>
    private float reduceCourseMovementSpeedDuration = 0.8f;

    private float increaseCourseMovementSpeedDuration = 0.8f;

    private float CourseMovementspeedTimer;

    void Awake() {
        //If there isn't a static instance yet, create it with this object
        if (instance == null) {
            instance = this;
        }

        //If there is an instance but this object is not it, remove it
        if (this != instance) {
            Destroy(gameObject);
        }

        normalCourseMovementSpeed = courseMovementSpeed;
    }

    void FixedUpdate() {
        course.Move(courseMovementSpeed);
    }

    void Update() {
        // If a player fell of the track
        if (players[0].transform.position.y < -15) Restart();
        if (players[1].transform.position.y < -15) Restart();

        if (IsTimeStopping && courseMovementSpeed > minCourseMovementSpeed) {
            CourseMovementspeedTimer += Time.unscaledDeltaTime;

            if (CourseMovementspeedTimer > reduceCourseMovementSpeedDuration) {
                CourseMovementspeedTimer = reduceCourseMovementSpeedDuration;
            }

            courseMovementSpeed = (float)Easing.ExpoEaseOut(CourseMovementspeedTimer, normalCourseMovementSpeed, minCourseMovementSpeed - normalCourseMovementSpeed, reduceCourseMovementSpeedDuration);
        } else if (!IsTimeStopping && courseMovementSpeed != normalCourseMovementSpeed) {
            CourseMovementspeedTimer += Time.unscaledDeltaTime;

            if (CourseMovementspeedTimer > increaseCourseMovementSpeedDuration) {
                CourseMovementspeedTimer = increaseCourseMovementSpeedDuration;
            }

            courseMovementSpeed = (float)Easing.ExpoEaseOut(CourseMovementspeedTimer, minCourseMovementSpeed, normalCourseMovementSpeed - minCourseMovementSpeed, increaseCourseMovementSpeedDuration);
        }
    }

    public Player GetPlayer(int index) {
        return players.Find(player => player.index == index);
    }

    public Player GetOtherPlayer(Player player) {
        return player.index == 0 ? GetPlayer(1) : GetPlayer(0);
    }

    public void Pickup(PickupType pickupType, Player player) {
        if (powerupType == PowerupType.pickupRumble) {
            StartCoroutine(RumblePattern((PlayerIndex)player.index, pickupType));
        } else {
            pickups.Add(pickupType);
        }
    }

    IEnumerator RumblePattern(PlayerIndex playerIndex, PickupType pickupType) {
        int rand = Random.Range(1, 5);

        switch(rand) {
            case 1: // long long
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(1.2f);
                RumbleStop(playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(1.2f);
                RumbleStop(playerIndex);
                pickups.Add(pickupType);
                pickupPattern.Add("11");
                break;
            case 2: // short short
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStop(playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStop(playerIndex);
                pickups.Add(pickupType);
                pickupPattern.Add("00");
                break;
            case 3: // long short
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(1.2f);
                RumbleStop(playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStop(playerIndex);
                pickups.Add(pickupType);
                pickupPattern.Add("10");
                break;
            case 4: // short long
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStop(playerIndex);
                yield return new WaitForSeconds(0.5f);
                RumbleStart(1, 0, playerIndex);
                yield return new WaitForSeconds(1.2f);
                RumbleStop(playerIndex);
                pickups.Add(pickupType);
                pickupPattern.Add("01");
                break;
        }
    }

    public bool HasPickup(PickupType pickupType) {
        return pickups.Contains(pickupType);
    }

    public void UsePickup(PickupType pickupType) {
        pickups.Remove(pickupType);
    }

    public void UsePickup(int index) {
        pickups.Remove(pickups[index]);
        pickupPattern.Remove(pickupPattern[index]);
    }

    public int PickupIndexWithPattern(PickupType pickupType, string pattern) {
        List<int> indices = new List<int>();
        for (int i = 0; i < pickupPattern.Count; i++) {
            if (pickupPattern[i] == pattern) {
                indices.Add(i);
            }
        }

        // We can use these to check if there are any pickups with same type in the list
        for (int y = 0; y < indices.Count; y++) {
            if (pickups[y] == pickupType) {
                return y;
            }
        }

        return -1;
     }

    public void SwapPlayerPositions() {
        Player PlayerOne = GetPlayer(0);
        Player PlayerTwo = GetPlayer(1);

        Vector3 playerOnePosition = PlayerOne.transform.position;
        Vector3 playerTwoPosition = PlayerTwo.transform.position;
        Vector3 playerOneVelocity = PlayerOne.GetComponent<Rigidbody>().velocity;
        Vector3 playerTwoVelocity = PlayerTwo.GetComponent<Rigidbody>().velocity;

        PlayerOne.SetPosition(playerTwoPosition);
        PlayerTwo.SetPosition(playerOnePosition);

        PlayerOne.GetComponent<Rigidbody>().velocity = playerTwoVelocity;
        PlayerTwo.GetComponent<Rigidbody>().velocity = playerOneVelocity;
    }

    public void StopTime() {
        IsTimeStopping = true;
    }

    public void ContinueTime() {
        IsTimeStopping = false;
    }

    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Rumble the controller for a certain amount of time
    /// </summary>
    /// <param name="left">Rumble for left motor, float between 1 and 0</param>
    /// <param name="right">Rumble for right motor, float between 1 and 0</param>
    /// <param name="time">The time it should rumble</param>
    public void Rumble(float left, float right, float time, PlayerIndex playerIndex) {
        GamePad.SetVibration(playerIndex, 0, 0);
        GamePad.SetVibration(playerIndex, left, right);
        StartCoroutine(StopRumbleAfterTime(time, playerIndex));
    }

    /// <summary>
    /// Start rumble. Don't forget to stop it.
    /// </summary>
    /// <param name="left">Rumble for left motor, float between 1 and 0</param>
    /// <param name="right">Rumble for right motor, float between 1 and 0</param>
    public void RumbleStart(float left, float right, PlayerIndex playerIndex) {
        GamePad.SetVibration(playerIndex, left, right);
    }

    /// <summary>
    /// Stop the rumble
    /// </summary>
    public void RumbleStop(PlayerIndex playerIndex) {
        GamePad.SetVibration(playerIndex, 0, 0);
    }

    /// <summary>
    /// IEnumerator to start as coroutine to stop the rumble after a certain amount of time.
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator StopRumbleAfterTime(float time, PlayerIndex playerIndex) {
        yield return new WaitForSeconds(time);
        RumbleStop(playerIndex);
    }
}
