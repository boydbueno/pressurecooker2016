﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;
using UnityEngine.SceneManagement;

public class ControlsManager : MonoBehaviour {

    /// <summary>
    /// Quick way to try out different controls
    /// </summary>
    public ControlsType controlsType;

    public enum ControlsType {
        keyboard,
        controllers,
        keyboardAndController
    }

    private List<bool> isHorizontalUsable = new List<bool>();
    private List<bool> isVerticalUsable = new List<bool>();

    private List<float> swapPressTime = new List<float>(new float[] { 0, 0 });
    private List<float> resizePressTime = new List<float>(new float[] { 0, 0 });
    private List<float> timeStopPressTime = new List<float>(new float[] { 0, 0 });

    public List<string> pressedSwapPattern = new List<string>();
    public List<string> pressedResizePattern = new List<string>();
    public List<string> pressedTimeStopPattern = new List<string>();

    private List<bool> swapLongPressFlag = new List<bool>();
    private List<bool> resizeLongPressFlag = new List<bool>();
    private List<bool> timeStopLongPressFlag = new List<bool>();

    private float patternRememberDuration = 3f;

    private List<Player> players = new List<Player>();

    private float syncDelay = 0.3f;
    private float rumbleSyncDelay = 0.6f;

    private int swapInitiatedPlayerIndex = -1;
    private float swapInitiatedTime = 0;

    private int resizeInitiatedPlayerIndex = -1;
    private float resizeInitiatedTime = 0;
    private float resizeDuration = 4f;

    private int timeStopInitiatedPlayerIndex = -1;
    private float timeStopInitiatedTime = 0;
    private float timeStopDuration = 3f;

    void Start() {
        players = GameManager.instance.players;

        for (int i = 0; i <= players.Count; i++) {
            isHorizontalUsable.Add(true);
            isVerticalUsable.Add(true);
        }

        pressedSwapPattern.Add("");
        pressedSwapPattern.Add("");
        pressedResizePattern.Add("");
        pressedResizePattern.Add("");
        pressedTimeStopPattern.Add("");
        pressedTimeStopPattern.Add("");

        swapLongPressFlag.Add(false);
        swapLongPressFlag.Add(false);
        resizeLongPressFlag.Add(false);
        resizeLongPressFlag.Add(false);
        timeStopLongPressFlag.Add(false);
        timeStopLongPressFlag.Add(false);
    }

    void Update() {
        switch (controlsType) {
            case ControlsType.keyboard:
                handleKeyboardControls();
                break;
            case ControlsType.controllers:
                handleControllerControls();
                break;
            case ControlsType.keyboardAndController:
                handleKeyboardAndControllerControls();
                break;
        }

        if (Input.GetKeyDown(KeyCode.F1)) {
            SceneManager.LoadScene("level1");
        }

        if (Input.GetKeyDown(KeyCode.F2)) {
            SceneManager.LoadScene("level2");
        }

        if (Input.GetKeyDown(KeyCode.F3)) {
            SceneManager.LoadScene("level3");
        }

        if (Input.GetKeyDown(KeyCode.F4)) {
            SceneManager.LoadScene("level4");
        }

        if (Input.GetKeyDown(KeyCode.F5)) {
            SceneManager.LoadScene("level5");
        }

        if (Input.GetKeyDown(KeyCode.F6)) {
            SceneManager.LoadScene("level6");
        }

        // We clear the patterns if the last press was more then patternRememberDuration ago
        float currentTime = Time.unscaledTime;

        if (currentTime > swapPressTime[0] + patternRememberDuration && swapPressTime[0] != 0) {
            swapPressTime[0] = 0;
            pressedSwapPattern[0] = "";
        }

        if (currentTime > swapPressTime[1] + patternRememberDuration && swapPressTime[1] != 0) {
            swapPressTime[1] = 0;
            pressedSwapPattern[1] = "";
        }

        if (currentTime > resizePressTime[0] + patternRememberDuration && resizePressTime[0] != 0) {
            resizePressTime[0] = 0;
            pressedResizePattern[0] = "";
        }

        if (currentTime > resizePressTime[1] + patternRememberDuration && resizePressTime[1] != 0) {
            resizePressTime[1] = 0;
            pressedResizePattern[1] = "";
        }

        if (currentTime > timeStopPressTime[0] + patternRememberDuration && timeStopPressTime[0] != 0) {
            timeStopPressTime[0] = 0;
            pressedTimeStopPattern[0] = "";
        }

        if (currentTime > timeStopPressTime[1] + patternRememberDuration && timeStopPressTime[1] != 0) {
            timeStopPressTime[1] = 0;
            pressedTimeStopPattern[1] = "";
        }

        if (GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble) {
            // Let's check if we are currently pressing any buttons into the 'long' press.
            if (Time.unscaledTime > swapPressTime[0] + 0.5f && swapPressTime[0] != 0 && !swapLongPressFlag[0]) {
                swapLongPressFlag[0] = true;
                GameManager.instance.Rumble(0, 0.5f, 0.5f, 0);
            }

            if (Time.unscaledTime > swapPressTime[1] + 0.5f && swapPressTime[1] != 0 && !swapLongPressFlag[1]) {
                swapLongPressFlag[1] = true;
                GameManager.instance.Rumble(0, 0.5f, 0.5f, (PlayerIndex)1);
            }

            if (Time.unscaledTime > resizePressTime[0] + 0.5f && resizePressTime[0] != 0 && !resizeLongPressFlag[0]) {
                resizeLongPressFlag[0] = true;
                GameManager.instance.Rumble(0, 0.5f, 0.5f, 0);
            }

            if (Time.unscaledTime > resizePressTime[1] + 0.5f && resizePressTime[1] != 0 && !resizeLongPressFlag[1]) {
                resizeLongPressFlag[1] = true;
                GameManager.instance.Rumble(0, 0.5f, 0.5f, (PlayerIndex)1);
            }

            if (Time.unscaledTime > timeStopPressTime[0] + 0.5f && timeStopPressTime[0] != 0 && !timeStopLongPressFlag[0]) {
                timeStopLongPressFlag[0] = true;
                GameManager.instance.Rumble(0, 0.5f, 0.5f, 0);
            }

            if (Time.unscaledTime > timeStopPressTime[1] + 0.5f && timeStopPressTime[1] != 0 && !timeStopLongPressFlag[1]) {
                timeStopLongPressFlag[1] = true;
                GameManager.instance.Rumble(0, 0.5f, 0.5f, (PlayerIndex)1);
            }
        }

    }

    private void Swap(int index) {
        if (GameManager.instance.powerupType != GameManager.PowerupType.always && !GameManager.instance.HasPickup(GameManager.PickupType.swap)) {
            return;
        }

        float delay = GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble ? rumbleSyncDelay : syncDelay; 

        if (index != swapInitiatedPlayerIndex && swapInitiatedTime + delay >= Time.time) {
            GameManager.instance.SwapPlayerPositions();
            swapInitiatedPlayerIndex = -1;
            swapInitiatedTime = 0;

            if (GameManager.instance.powerupType != GameManager.PowerupType.always && GameManager.instance.powerupType != GameManager.PowerupType.pickupRumble) {
                GameManager.instance.UsePickup(GameManager.PickupType.swap);
            }

            if (GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble) {
                int powerUpIndex = GameManager.instance.PickupIndexWithPattern(GameManager.PickupType.swap, pressedSwapPattern[index]);
                GameManager.instance.UsePickup(powerUpIndex);
            }

            return;
        }

        swapInitiatedPlayerIndex = index;
        swapInitiatedTime = Time.time;
    }

    private void Resize(int index) {
        if (GameManager.instance.powerupType != GameManager.PowerupType.always && !GameManager.instance.HasPickup(GameManager.PickupType.resize)) {
            return;
        }

        float delay = GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble ? rumbleSyncDelay : syncDelay;

        if (index != resizeInitiatedPlayerIndex && resizeInitiatedTime + delay >= Time.time) {
            resizeInitiatedPlayerIndex = -1;
            resizeInitiatedTime = 0;

            // Enlarge a player and make a player smaller
            int random = Random.Range(0, 2);

            if (random < 1f) {
                players[0].Enlarge();
                players[1].Shrink();
            } else {
                players[1].Enlarge();
                players[0].Shrink();
            }

            if (GameManager.instance.powerupType != GameManager.PowerupType.always && GameManager.instance.powerupType != GameManager.PowerupType.pickupRumble) {
                GameManager.instance.UsePickup(GameManager.PickupType.resize);
            }

            if (GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble) {
                int powerUpIndex = GameManager.instance.PickupIndexWithPattern(GameManager.PickupType.resize, pressedResizePattern[index]);
                GameManager.instance.UsePickup(powerUpIndex);
            }

            StartCoroutine(EndResize(resizeDuration));

            return;
        }

        resizeInitiatedPlayerIndex = index;
        resizeInitiatedTime = Time.time;
    }

    IEnumerator EndResize(float time) {
        yield return new WaitForSeconds(time);

        players[0].ReturnToNormal();
        players[1].ReturnToNormal();
    }

    private void TimeStop(int index) {
        if (GameManager.instance.powerupType == GameManager.PowerupType.pickup && !GameManager.instance.HasPickup(GameManager.PickupType.timeStop)) {
            return;
        }

        float delay = GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble ? rumbleSyncDelay : syncDelay;

        if (index != timeStopInitiatedPlayerIndex && timeStopInitiatedTime + delay >= Time.time) {
            GameManager.instance.StopTime();

            timeStopInitiatedPlayerIndex = -1;
            timeStopInitiatedTime = 0;

            if (GameManager.instance.powerupType == GameManager.PowerupType.pickup) {
                GameManager.instance.UsePickup(GameManager.PickupType.timeStop);
            }

            if (GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble) {
                int powerUpIndex = GameManager.instance.PickupIndexWithPattern(GameManager.PickupType.timeStop, pressedTimeStopPattern[index]);
                GameManager.instance.UsePickup(powerUpIndex);
            }

            StartCoroutine(EndTimeStop(timeStopDuration));

            return;
        }

        timeStopInitiatedPlayerIndex = index;
        timeStopInitiatedTime = Time.time;
    }

    IEnumerator EndTimeStop(float time) {
        yield return new WaitForSeconds(time);

        GameManager.instance.ContinueTime();
    }

    private void Jump(int index) {
        GameManager.instance.GetPlayer(index).Jump();
    }

    private void Right(int index) {
        if (GameManager.instance.GetPlayer(index).transform.position.x == 45) {
            return;
        }

        Player player = GameManager.instance.GetPlayer(index);

        player.MoveRight();
    }

    private void Left(int index) {
        if (GameManager.instance.GetPlayer(index).transform.position.x == -45) {
            return;
        }

        Player player = GameManager.instance.GetPlayer(index);

        player.MoveLeft();
    }

    private void Up(int index) {
        Debug.Log("Player " + index + " pressed up");
    }

    private void Down(int index) {
        Debug.Log("Player " + index + " pressed down");
    }

    private void handleKeyboardControls() {
        KeyboardControls(0);
        KeyboardControls(1);
    }

    private void handleControllerControls() {
        ControllerControls(0);
        ControllerControls(1);
    }

    private void handleKeyboardAndControllerControls() {
        KeyboardControls(1);
        ControllerControls(0);
    }

    private void KeyboardControls(int i) {
        if (Input.GetButtonDown("Left" + i)) {
            Left(i);
        } else if (Input.GetButtonDown("Right" + i)) {
            Right(i);
        } else if (Input.GetButtonDown("Up" + i)) {
            Up(i);
        } else if (Input.GetButtonDown("Down" + 1)) {
            Down(i);
        }

        GenericButtonHandling(i);
    }

    private void ControllerControls(int i) {
        if (Input.GetAxis("Horizontal" + i) == -1 && isHorizontalUsable[i]) {
            Left(i);
            isHorizontalUsable[i] = false;
        } else if (Input.GetAxis("Horizontal" + i) == 1 && isHorizontalUsable[i]) {
            Right(i);
            isHorizontalUsable[i] = false;
        } else if (Input.GetAxis("Vertical" + i) == 1 && isVerticalUsable[i]) {
            Up(i);
            isVerticalUsable[i] = false;
        } else if (Input.GetAxis("Vertical" + i) == -1 && isVerticalUsable[i]) {
            Down(i);
            isVerticalUsable[i] = false;
        }

        GenericButtonHandling(i);

        if (Input.GetAxis("Horizontal" + i) == 0) {
            isHorizontalUsable[i] = true;
        }

        if (Input.GetAxis("Vertical" + i) == 0) {
            isVerticalUsable[i] = true;
        }
    }

    private void GenericButtonHandling(int i) {
        if (Input.GetButtonDown("Jump" + i)) {
            Jump(i);
        }

        if (Input.GetButtonDown("Swap" + i)) {
            swapPressTime[i] = Time.unscaledTime;

            if (GameManager.instance.powerupType != GameManager.PowerupType.pickupRumble) {
                Swap(i);
            }
        }

        if (Input.GetButtonDown("Resize" + i)) {
            resizePressTime[i] = Time.unscaledTime;

            if (GameManager.instance.powerupType != GameManager.PowerupType.pickupRumble) {
                Resize(i);
            }
        }
       

        if (Input.GetButtonDown("TimeStop" + i)) {
            timeStopPressTime[i] = Time.unscaledTime;

            if (GameManager.instance.powerupType != GameManager.PowerupType.pickupRumble) {
                TimeStop(i);
            }
        }

        if (GameManager.instance.powerupType == GameManager.PowerupType.pickupRumble) {

            if (Input.GetButtonUp("Swap" + i)) {
                float pressTime = Time.unscaledTime - swapPressTime[i];
                swapPressTime[i] = 0;
                swapLongPressFlag[i] = false;

                if (pressedSwapPattern[i].Length == 2) {
                    pressedSwapPattern[i] = pressedSwapPattern[i].Substring(1, 1);
                }

                pressedSwapPattern[i] += translatePressLength(pressTime);

                if (pressedSwapPattern[i].Length == 2) {
                    int powerUpIndex = GameManager.instance.PickupIndexWithPattern(GameManager.PickupType.swap, pressedSwapPattern[i]);
                    if (powerUpIndex == -1) {
                        // Wrong pattern
                    } else {
                        Swap(i);
                        //GameManager.instance.UsePickup(powerUpIndex);
                    }
                }
            }

            if (Input.GetButtonUp("Resize" + i)) {
                float pressTime = Time.unscaledTime - resizePressTime[i];
                resizePressTime[i] = 0;
                resizeLongPressFlag[i] = false;

                if (pressedResizePattern[i].Length == 2) {
                    pressedResizePattern[i] = pressedResizePattern[i].Substring(1, 1);
                }

                pressedResizePattern[i] += translatePressLength(pressTime);

                if (pressedResizePattern[i].Length == 2) {
                    int powerUpIndex = GameManager.instance.PickupIndexWithPattern(GameManager.PickupType.resize, pressedResizePattern[i]);
                    if (powerUpIndex == -1) {
                        // Wrong pattern
                    } else {
                        Resize(i);
                        //GameManager.instance.UsePickup(powerUpIndex);
                    }
                }
            }

            if (Input.GetButtonUp("TimeStop" + i)) {
                float pressTime = Time.unscaledTime - timeStopPressTime[i];
                timeStopPressTime[i] = 0;
                timeStopLongPressFlag[i] = false;

                if (pressedTimeStopPattern[i].Length == 2) {
                    pressedTimeStopPattern[i] = pressedTimeStopPattern[i].Substring(1, 1);
                }

                pressedTimeStopPattern[i] += translatePressLength(pressTime);

                if (pressedTimeStopPattern[i].Length == 2) {
                    int powerUpIndex = GameManager.instance.PickupIndexWithPattern(GameManager.PickupType.timeStop, pressedTimeStopPattern[i]);
                    if (powerUpIndex == -1) {
                        // Wrong pattern
                    } else {
                        TimeStop(i);
                    }
                }
            }
        }
    }

    private string translatePressLength(float length) {
        if (length < 0.5f) {
            return "0";
        } else {
            return "1";
        }
    }
}
