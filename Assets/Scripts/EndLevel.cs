﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour {

    public string nextScene;

    void OnTriggerEnter(Collider collider) {
        SceneManager.LoadScene(nextScene);
    }
}
